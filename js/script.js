function checkIfArray(arr) {
    var total = 0
    if (Array.isArray(arr)) {
        arr.forEach(element => {
            total += element
        });
    } else {
        total = arr
    }
    return total
}
function smallestValIndex(arr) {
    var index = 0;
    var value = arr[0];
    for (var i = 1; i < arr.length; i++) {
        if (arr[i] < value) {
            value = arr[i];
            index = i;
        }
    }
    return index
}
function smallestVal(arr) {
    var index = 0;
    var value = arr[0];
    for (var i = 1; i < arr.length; i++) {
        if (arr[i] < value) {
            value = arr[i];
            index = i;
        }
    }
    return value

}

function largestValue(arr) {
    return Math.max.apply(Math, arr)
}

function slick_standard(arr) {

    let totalHeight = checkIfArray(arr)
    totalHeight -= 30

    let max610 = (totalHeight - 1235) / 610
    let max500 = (totalHeight - 1235) / 500
    let min610 = (totalHeight - 600) / 610
    let min500 = (totalHeight - 600) / 500
    
    let max610liekana = (totalHeight - 1235) % 610 // kuo arciau 610, tuo maziau pjaut
    let max500liekana = (totalHeight - 1235) % 500
    let min610liekana = (totalHeight - 600) % 610 // kuo arciau 610
    let min500liekana = (totalHeight - 600) % 500

    let max610Decimal = max610 - Math.floor(max610)
    let max500Decimal = max500 - Math.floor(max500)
    let min610Decimal = min610 - Math.floor(min610)
    let min500Decimal = min500 - Math.floor(min500)

    let max610Skirtumas = 610 * max610Decimal 
    let min610Skirtumas = 610 * min610Decimal 
    let max500Skirtumas = 500 * max500Decimal  
    let min500Skirtumas = 500 * min500Decimal 

    let max610Galas = (1235 - (610 - max610Skirtumas)) / 2
    let max500Galas = (1235 - (500 - max500Skirtumas)) / 2
    let min610Galas = (600 + min610Skirtumas) / 2
    let min500Galas = (600 + min500Skirtumas) / 2
    
    let max610proc = Math.abs(610 - max610Galas) / 6.1
    let max500proc = Math.abs(500 - max500Galas) / 5
    let min610proc = Math.abs(610 - min610Galas) / 6.1
    let min500proc = Math.abs(500 - min500Galas) / 5

    let sgmnSkMax610 = Math.ceil((totalHeight - 1235) / 610) + 2
    let sgmnSkMax500 = Math.ceil((totalHeight - 1235) / 500) + 2
    let sgmnSkMin610 = Math.floor((totalHeight - 600) / 610) + 2
    let sgmnSkMin500 = Math.floor((totalHeight - 600) / 500) + 2

    let max610Arr = []
    let max500Arr = []
    let min610Arr = []
    let min500Arr = []
    
    if (max610Galas * 2 === 625 && sgmnSkMax610 >= 3) {
        max610Arr.push(625)
        for(let i = 0; i < sgmnSkMax610 - 2; i++) {
            max610Arr.push(610)
        }
        max610Arr.push(610)
    } else {
        max610Arr.push(max610Galas)
        for(let i = 0; i < sgmnSkMax610 - 2; i++) {
            max610Arr.push(610)
        }
        max610Arr.push(max610Galas)
    }

    max500Arr.push(max500Galas)
    for(let i = 0; i < sgmnSkMax500 - 2; i++) {
        max500Arr.push(500)
    }
    max500Arr.push(max500Galas)
    
    min610Arr.push(min610Galas)
    for(let i = 0; i < sgmnSkMin610 - 2; i++) {
        min610Arr.push(610)
    }
    min610Arr.push(min610Galas)

    min500Arr.push(min500Galas)
    for(let i = 0; i < sgmnSkMin500 - 2; i++) {
        min500Arr.push(500)
    }
    min500Arr.push(min500Galas)

    
    let procArr = []
    procArr.push(max610proc)
    procArr.push(max500proc)
    procArr.push(min610proc)
    procArr.push(min500proc)

    let issidestymai = []
    issidestymai.push(max610Arr)
    issidestymai.push(max500Arr)
    issidestymai.push(min610Arr)
    issidestymai.push(min500Arr)

    issidestymai.forEach(el => {
        if ((el[0] >= 485 && el[0] <= 507.5) || (el[0] >= 595 && el[0] <= 617.5)) {
            if (el[0] >= 485 && el[0] <= 507.5) {
                el[0] = (el[0] * 2) - 500
                el[el.length - 1] = 500
            } else {
                el[0] = (el[0] * 2) - 610
                el[el.length - 1] = 610
            }
        }
    })
    issidestymai.forEach(el => {
        if (el[el.length - 1] >= 595) {
            let skirtumas = 610 - el[el.length - 1]
            el[0] -= skirtumas
            el[el.length - 1] = 610
        } else if (el[el.length - 1] >= 485 && el[el.length - 1] <= 515) {
            let skirtumas
            if (el[el.length - 1] < 500) {
                skirtumas = 500 - el[el.length - 1]
                el[0] -= skirtumas
            } else if (el[el.length - 1] > 500) {
                skirtumas = Math.abs(500 - el[el.length - 1])
                el[0] += skirtumas
            }
            el[el.length - 1] = 500
        }
        
        // tikrina ar turi skaiciu po kableliais
        if (el[el.length - 1] % 1 !== 0) {
            // console.log('apvalinamas paskutinis elementas')
            el[el.length - 1] = Math.round(el[el.length - 1])
            let sumaBeVirsaus = 0
            el.forEach((element, index) => {
                if (index !==0) {
                    sumaBeVirsaus += element
                }
            })
            el[0] = totalHeight - sumaBeVirsaus
        } else if (el[0] % 1 !== 0) {
            // skaiciuojant 2032 auksti, gunamas virsutinio el aukstis 501.999
            el[0] = Math.round(el[0])
        }
        
    })


    var index = 0;
    var value = procArr[0];
    for (var i = 1; i < procArr.length; i++) {
        if (procArr[i] < value) {
            value = procArr[i];
            index = i;
        }
    }
    let graziausiasIsdestymas = issidestymai[smallestValIndex(procArr)]
    
    let layoutsAndPercents = []
    for(let i = 0; i < issidestymai.length; i++) {
        layoutsAndPercents[i] = {   
            'layout': issidestymai[i], 
            'percent': procArr[i], 
            'narrowPanels' : function() {
                let count = 0
                this.layout.forEach((el, index) => {
                    if (index !== 0) {
                        if (el <= 500) count++
                    } else {
                        // PASTABA: pasitikrint iki kokio aukscio pirmas segmentas dar skaitomas siauru. Man rodos 500 + 15 snapas
                        if (el <= 515) {
                            count++
                        }
                    }
                })
                return count
            },
            'panelCount': issidestymai[i].length
    
        }
    }

    
    let sortedByPercent = layoutsAndPercents.sort((a, b) => {
        return a.percent - b.percent
    })

    let panelCount = []
    layoutsAndPercents.forEach(el => {
        panelCount.push(el.panelCount)
    })
    let leastPanelCount = smallestVal(panelCount)

    let narrowPanelCount = []
    layoutsAndPercents.forEach(el => {
        if (el.panelCount <= leastPanelCount) {
            narrowPanelCount.push(el.narrowPanels())
        }
    })
    let mostNarrowPanels = largestValue(narrowPanelCount)

    let cheapest = sortedByPercent.find(el => {
        if (el.narrowPanels() >= mostNarrowPanels) {
            if (el.panelCount <= leastPanelCount) {
                if ((el.layout[el.layout.length - 1] === 500 || el.layout[el.layout.length - 1] === 610)) {
                    return el
                }
            }
        }
    })
    
    if (cheapest === undefined) {
        cheapest = sortedByPercent.find(el => {
            if (el.narrowPanels() >= mostNarrowPanels) {
                if (el.panelCount <= leastPanelCount) {
                    return el
                }
            }
        })
    }

    let patikrinimas = 30
    graziausiasIsdestymas.forEach(el => {
        patikrinimas+= el
    })

    return graziausiasIsdestymas     
}

(function(window) {
    window.windowSVG = ''
    window.segmentai = {
        segmentuTipai: ['slick', 'slick', 'slick', 'slick'],
        segmentuAuksciai: [500, 500, 500, 610],
        langai: [],
        segmentuPlotis: 2000,
        vartuAukstis: 2000,
            
    }

    window.panels = {
        initialPanelType: '',
        active: '',
        activeNo : '',
        activeNoCalc: function(index) {
            this.activeNo = index
        },
        activePanelHeight : function() {
            return this.active.getAttribute('height')
        },
        activePanelType: function() {
            if (this.activeNo !== '') {
                let typeFromAttribute = document.getElementById('panels').childNodes[this.activeNo].getAttribute('data-panel-type')
                if (typeFromAttribute == null) {
                    return this.initialPanelType
                } else {
                    return typeFromAttribute
                }
            } else if (this.initialPanelType !== null) {
                // // console.log(this.initialPanelType )
                return this.initialPanelType 
            } else {
                // // console.log(this.initialPanelType, this.activeNo)
            }
    
        },
        activePanelHeight: function() {
            if (this.activeNo >= 0) {
                let scaledHeight =  document.getElementById('panels').childNodes[this.activeNo].getAttribute('height')
                return scaledHeight / scale
            } else {
                return 500
            }
        },
        type: ''
    }
    window.doors = {
        widthInput: 3000,
        heightInput: 3000,
        rubberH: 30
    }
    
    window.windowDimensions = {
        // pastaba: palikti tik sita
        tarpas2: function(windowsOnAPanel) {
            let panelW = doors.widthInput
            let spaces = windowsOnAPanel + 1
            let spaceW = (panelW - windowsOnAPanel * this.windowW)/spaces
            return spaceW
        },
        standard : {
            w: 488,
            h: 322
        },
        cross: {
            w: 488,
            h: 322
        },
        round: {
            // pastaba: taisyti
            small: {w: 154},
            medium : {w: 234},
            large: {w: 314}
        }
    }    
})(window)

function updatePanels(dom_id) {
    document.getElementById('panels').innerHTML = ''
    // scale = scaleCoef(400, 400, doors.widthInput, 500)

    // let count = segmentai.segmentuTipai.length
    let count = segmentai.segmentuAuksciai.length

    for (let i = 0; i < count; i++) {      
        if (dom_id === 'panels') {
            createSvgDOMTree(i)
        } else {
            // pastaba: sitas nelabai naudojamas
            // console.log(segmentai.segmentuTipai[i])
            if (i === 0) {
                window['create_'+ segmentai.segmentuTipai[i] +'_panel'](dom_id, segmentai.segmentuAuksciai[i], doors.widthInput, true)
            } else {
                window['create_'+ segmentai.segmentuTipai[i] +'_panel'](dom_id, segmentai.segmentuAuksciai[i], doors.widthInput, false)
            }
        }
    }
    if (dom_id === 'panels' && doors.widthInput) {
        // inserts one aditional div in 'afterSVG' div to display width
       document.getElementsByClassName('afterSVG')[(segmentai.segmentuTipai.length - 2)].insertAdjacentHTML('beforeend', `<div class="afterSVGwidth">${doors.widthInput}</div>`)
       document.getElementsByClassName('afterSVGwidth')[0].style.left = `${(doors.widthInput * scale)/2}px`
    }
    if (segmentai.langai !== []) {
        // updateWindows()
    }
    // rubber_standard('panels', doors.widthInput)
}

function createSvgDOMTree(i) {
    let panelsNode = document.getElementById('panels')
    
    let svgContainer = document.createElement('div')
    svgContainer.setAttribute('id', `panelContainer-${i}`)
    svgContainer.setAttribute('class', `panelContainer`)
    panelsNode.appendChild(svgContainer)
    // pastaba: jei segmentai topribiniai, tai pakeisti pirmo segmento auksti. Jis turi buti 10cm mazesnis uz paskutini
    if (i === 0) {
        window['create_'+ segmentai.segmentuTipai[i] +'_panel'](`panelContainer-${i}`, segmentai.segmentuAuksciai[i], doors.widthInput, true)
    } else {
        window['create_'+ segmentai.segmentuTipai[i] +'_panel'](`panelContainer-${i}`, segmentai.segmentuAuksciai[i], doors.widthInput, false)
    }
    // let panelsNode = document.getElementById('panels')
    let afterSVGDIV = document.createElement('div')
    afterSVGDIV.setAttribute('class', 'afterSVG')

    // cia gali buti klaida del keiciamo input
    let afterSVGHeight = document.createElement('div')
    afterSVGHeight.setAttribute('class', 'afterSVGHeight')
    afterSVGHeight.innerHTML = `${segmentai.segmentuAuksciai[i]}`
    afterSVGHeight.style.top = `-${(segmentai.segmentuAuksciai[i])/2 * scale}px`
    
    svgContainer.appendChild(afterSVGDIV)
    afterSVGDIV.appendChild(afterSVGHeight)
}



let request = new XMLHttpRequest();

request.open('GET', '../data.json', true)
request.onload = function () {
    // Convert JSON data to an object
    let data = JSON.parse(this.response)
    console.log(data.height)
    segmentai.segmentuAuksciai = slick_standard(data.height)

    let panelsCount = Object.keys(data.panels).length
    let leaves = []
    for (let i = 1; i <= panelsCount; i++) {
        leaves.push(data.panels[i].leaf)
    }
    segmentai.segmentuTipai = leaves

    doors.widthInput = data.width
    updatePanels('panels')
}

request.send()
