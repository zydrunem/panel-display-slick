
var col = {
    red: '#e34d69',
    blue: '#bdc5c9',
    rib: '#B8B8B8',
    RAL9002 : '#d7d5cb',
    frame : '#6b6b6b',
    frame1 : '#8c8c8c',
    frameLighter : '#a6a6a6',
    glass: '#bddbff',
    outline: '#fff',
    offwhite: '#e0e0e0'
}

let wPanel = 4000
let hPanel500 = 500

function scaleCoef(wPort, hPort, wEl, hEl) {
    // c - coefficient
    let cWidth = wPort / wEl
    let cHeight = hPort / hEl
    let coef
    if (cWidth <= cHeight) coef = cWidth
    if (cHeight <= cWidth) coef = cHeight

    console.log(`koeficientas: ${coef}`)
    return coef
}

// scale = scaleCoef(500, 500, 2000, hPanel500)
let scale = 0.25

// funkcija ideda elementa paneliu virsuj. Visoms, iskyrus virsutine
function add_top_space(topPanel, wPanel, nestedEl) {
    let hSpaceTop = 3 * scale
    if (!topPanel) {
        nestedEl.rect(wPanel, hSpaceTop).attr({
            fill: col.red, 
            x: 0,
            y: 0
        })
    }
}

// funkcija ideda visoms panelems tarpa apacioje
function add_bottom_space(wPanel, hPanel, nestedEl) {
    let hSpaceBottom = 13 * scale
    nestedEl.rect(wPanel, hSpaceBottom).attr({
        fill: col.blue,
        x: 0,
        y: (hPanel - hSpaceBottom),
    })
}
function create_slick_panel(dom_id, hPanel, wPanel, topPanel = false) {
    wPanel *= scale
    hPanelScaled = hPanel * scale

    var svgCont = SVG(dom_id).size(wPanel, hPanelScaled).attr({
        class: ['slick', 'panel'],
        'data-panel-type': 'slick',
        'data-panel-height': hPanel
    })
    var panel = svgCont.rect(wPanel, hPanelScaled).attr({ 
        fill: '#d7d5cb',
        y: 0,
        x: 0
    })

    var nested = svgCont.nested()
    add_top_space(topPanel, wPanel, nested)
    add_bottom_space(wPanel, hPanelScaled, nested)
}


let cassetteCoord = []
function create_cassette_panel(dom_id, hPanel, wPanel, topPanel = false) {
    if (hPanel < 465) console.log('filinginiai pjaunami iki 465 mm')
    wPanel *= scale
    let hPanelScaled = hPanel * scale
    let hCassette = 346
    var svgCont = SVG(dom_id).size(wPanel, hPanelScaled).attr({
        class: ['cassette', 'panel'],
        'data-panel-type': 'cassette',
        'data-panel-height': hPanel
    })
    var panel = svgCont.rect(wPanel, hPanelScaled).attr({ 
        fill: '#d7d5cb',
        y: 0,
        x: 0
    })

    var group = svgCont.group().attr({
        class: 'cassettePanel',
    })

    var style = {
        fill: 'none',
        stroke: '#000000',
        strokeWidth: 0.25
    }
    group.path('M509 346H3c-1.7 0-3-1.3-3-3V3c0-1.7 1.3-3 3-3h506c1.7 0 3 1.3 3 3v340C512 344.7 510.7 346 509 346z').attr(style)
    group.path('M502 339H10c-1.7 0-3-1.3-3-3V10c0-1.7 1.3-3 3-3h492c1.7 0 3 1.3 3 3v326C505 337.7 503.7 339 502 339z').attr(style)
    group.path('M497 335H15c-1.7 0-3-1.3-3-3V14c0-1.7 1.3-3 3-3h482c1.7 0 3 1.3 3 3v318C500 333.7 498.7 335 497 335z').attr(style)
    group.path('M487 324.1H25c-1.7 0-3-1.3-3-3V24.9c0-1.7 1.3-3 3-3h462c1.7 0 3 1.3 3 3v296.3C490 322.8 488.7 324.1 487 324.1z').attr(style)
    // vidurinis staciakampis
    group.rect(390, 226).attr(style).attr({x: 61, y: 60})
    group.line(61, 286, 24, 323).attr(style)
    group.line(451, 286, 488, 323).attr(style)
    group.line(451, 60, 488, 23).attr(style)
    group.line(61, 60, 23.5, 22.5).attr(style)

    // scaleGroup('cassettePanel')
    let wPanelInitial = wPanel / 0.25
    let cassetteCount = 3
    let tarpasTarp = 100
    if (wPanelInitial < 2000) {
        cassetteCount = 0
        console.log('filinginiams vartams paisyti, panele yra per siaura')
    }
    if (wPanelInitial > 2450) {
        cassetteCount = 4
    } 
    if (wPanelInitial > 2850) {
        cassetteCount = 4
    }
    if (wPanelInitial > 3050) {
        cassetteCount = 5
    } 
    if (wPanelInitial > 3650) {
        cassetteCount = 6
    } 
    if (wPanelInitial > 4250) {
        cassetteCount = 7
    } 
    if (wPanelInitial > 4850) {
        cassetteCount = 8
    } 
    if (wPanelInitial > 5470) {
        cassetteCount = 9
    } 
    if (cassetteCount === 3 || cassetteCount === 4 && wPanelInitial > 2850) {
        tarpasTarp = 150
    }
    // koordinates 
    // let cassetteCoord = []
    // pastaba: pasitiklinti del plocio
    cassetteCoord[0] = (wPanelInitial - ((cassetteCount * 512) + (cassetteCount-1) * tarpasTarp)) / 2

    for (let i = 1; i < cassetteCount; i++) {
        // koordinates aktualios dedant langus
        cassetteCoord[i] = (cassetteCoord[i - 1]) + 508 + tarpasTarp
    }

    // 346 yra sachmato aukstis, plotis - 512
    // let chessYPos = (hPanel - 346) / 2

    let chessYPos
    let spaceBeforeChess
    if (hPanel <= 500) {
        spaceBeforeChess = (500 - 346) / 2
    } else if (hPanel <= 610) {
        spaceBeforeChess = (610 - 346) / 2
    }

    if (topPanel) {
        chessYPos = hPanel - spaceBeforeChess - 346
    } else if (!topPanel) {
        chessYPos = spaceBeforeChess
    }

    group.node.style.transform = `matrix(1, 0, 0, 1, ${cassetteCoord[0] * scale}, ${chessYPos * scale}) scale(${scale})`
    // group.node.style.transform = `matrix(1, 0, 0, 1, ${cassetteCoord[0] * scale}, ${chessYPos * scale}) scale(${scale})`

    for(let i = 1; i < cassetteCount; i++) {
        svgCont.use(group).move((508 + tarpasTarp) * i * scale)
    }
    // jei ne viršutinė, turi viršutinį tarpą
    add_top_space(topPanel, wPanel, svgCont)
    add_bottom_space(wPanel, hPanelScaled, svgCont)
}
/*
function create_rib_panel(dom_id, hPanel, wPanel, topPanel = false) {
    wPanel *= scale
    hPanelScaled = hPanel * scale
    let hRib = 16
    hRib *= scale
    let hSpaceBottom = 13
    hSpaceBottom *= scale

    var svgCont = SVG(dom_id).size(wPanel, hPanelScaled).attr({
        class: 'rib'
    })
    var panel = svgCont.rect(wPanel, hPanelScaled).attr({ 
        fill: '#d7d5cb',
        y: 0,
        x: 0
    })

    var nested = svgCont.nested()

    if (hPanel === 500) {
        ribDistance = 100 * scale
        // for (let i = 100; i < 500; i += 100) {
        for (let i = ribDistance; i < hPanelScaled; i += ribDistance) {
            nested.rect(wPanel, hRib).attr({ 
                fill: col.rib,
                y:  (i - hRib/2)
            })
        }
    }

    if (hPanel === 610) {
        ribDistance = 100 * scale
        // for (let i = 100; i < 500; i += 100) {
        for (let i = ribDistance; i <= 400 * scale; i += ribDistance) {
            nested.rect(wPanel, hRib).attr({ 
                fill: col.rib,
                y:  (i - hRib/2)
            })
        }
        // paskutinis ribas, nes jis nutoles ne 100, o 110 atstumu nuo apacaios
        nested.rect(wPanel, hRib).attr({ 
            fill: col.rib,
            y:  (hPanel - 110) * scale - hRib/2
        })
    }
// console.log((hPanel - hSpaceBottom - hRib) * scale)
// console.log(hPanel, hSpaceBottom)
    let cutSegment 
    if (hPanel !== 500 & hPanel !== 610) {
        cutSegment = true
    }
    if (cutSegment) {
        var rib = nested.rect(wPanel, hRib).attr({
            class: 'specialLady',
            fill: col.rib,
            y: hPanelScaled - hSpaceBottom - hRib - 100 * scale
        })

        console.log(rib.y())

        // rib count on cut segment.
        let ribCount = Math.floor((hPanelScaled - hSpaceBottom)/ (116 * scale))
        console.log(ribCount)
        for (let i = 1; i < ribCount; i++) {
            nested.use(rib).move(0, (-116 * i * scale))
        }
    }

    // jei ne viršutinė, turi viršutinį tarpą
    add_top_space(topPanel, wPanel, nested)
    add_bottom_space(wPanel, hPanelScaled, nested)
}
*/
function create_rib_panel(dom_id, hPanel, wPanel, topPanel = false) {
    wPanel *= scale
    hPanelScaled = hPanel * scale
    let hRib = 16
    hRib *= scale
    let hSpaceBottom = 13
    hSpaceBottom *= scale

    var svgCont = SVG(dom_id).size(wPanel, hPanelScaled).attr({
        class: 'rib'
    })
    var panel = svgCont.rect(wPanel, hPanelScaled).attr({ 
        fill: '#d7d5cb',
        y: 0,
        x: 0
    })

    var nested = svgCont.nested()
    
    if (topPanel === true) {
        // ant virsutines paneles, ribai paisomi nuo apacioj, nes ji pjaunama. Visais kitais atvejais paisoma nuo virsaus
        let iskilimas = 84
        iskilimas *= scale

        if (hPanel <= 500) {
            var lastSpace = 87
            lastSpace *= scale
        } else if (hPanel > 500) {
            // vadinasi yra pjautas platus segmentas, vadinasi apatinis ribas nutoles 98 mm nuo apacios
            var lastSpace = 98
            lastSpace *= scale
        }
        var rib = nested.rect(wPanel, hRib).attr({
            class: 'specialLady',
            fill: col.rib,
            y: hPanelScaled - lastSpace - hRib
        })
        // is paneles aukscio, tai 500 ar maziau, atima pirmo iskilimo atstuma (87) + idubima (16) ir dalina is atstumu, koki uzima kiti iskilimai (84) + tarpas(16)
        let ribCount = Math.round((hPanelScaled - (lastSpace + hRib)) / (iskilimas + hRib))
        for (let i = 1; i <= ribCount; i++) {
            nested.use(rib).move(0, -100 * scale * i)
        }
    } else if (topPanel !== true) {
        // tuomet ribai piesiami nuo virsaus, kad nupjaunant paskutini segmenta, nebutu nupjaunamas atskaitos taskas
        let topSpace
        if (hPanel > 500) {
            // tuomet paneles aukstis be paskutinio ribo yra aukstis - 
            // pirmas ribas nuo virsaus nutoles 96mm
            topSpace = 96
        } else if (hPanel <= 500) {
            topSpace = 97
        }
        var rib = nested.rect(wPanel, hRib).attr({
            class: 'specialLady2',
            fill: col.rib,
            y: topSpace * scale
        })
        let likesAukstis = hPanel - topSpace - 16
        let ribCount = Math.floor(likesAukstis / 100)
        for (let i = 1; i <= ribCount; i++) {
            nested.use(rib).move(0, (84 * scale + hRib) * i )
            // nested.use(rib).move(0, (16 * scale) * i )
        }
    }
    
 
    add_top_space(topPanel, wPanel, nested)
    add_bottom_space(wPanel, hPanelScaled, nested)
}

/*
create_rib_panel('ribPanelTop', 500, topPanel = true)
create_rib_panel('ribPanel1', 500)
create_rib_panel('ribPanelBottom', 500)
*/
function create_midrib_panel(dom_id, hPanel, wPanel, topPanel = false) {
    // pastaba: pasitikslinti del 435 mm
    if (hPanel <= 435) console.log('midrib segmentai pjaunami iki 435mm')
    wPanel *= scale
    hPanelScaled = hPanel * scale
    hRib = 16
    hRib *= scale // 4
    let ribYPos 

    var svgCont = SVG(dom_id).size(wPanel, hPanelScaled).attr({
        class: ['midrib', 'panel'],
        'data-panel-type': 'midrib',
        'data-panel-height': hPanel
    })
    var panel = svgCont.rect(wPanel, hPanelScaled).attr({ 
        fill: col.RAL9002,
        y: 0,
        x: 0
    })

    var nested = svgCont.nested()
    // Jei daugiau, nei 500, vadinasi pjautas 610 segmentas- kitoj vietoj ribas. Jei pjautas ir virsuj, tai nutoles 360mm nuo virsaus, jei apacioj, tai 360 nuo apacios.
    if (hPanel > 500)  {
        if (topPanel) {
            // ribas nuo apacios, kad pjaunant virsuje, jo pozicija nepasikeistu
            ribYPos = hPanelScaled - (250 * scale) - (hRib / 2)
            console.log(ribYPos)
        } else if (!topPanel) {
            // ribas nuo virsaus, kad pjaunant apacia, jo pozicija nepasikeistu
            ribYPos = 250 * scale - (hRib / 2)
        }
    } else if (hPanel < 500 && hPanel >= 435) {
        if (topPanel) {
            ribYPos = hPanelScaled - (250 * scale) - (hRib / 2)
        } else if (!topPanel) {
            ribYPos = 250 * scale - (hRib / 2) 
        }
    } else if (hPanel === 500) {
        ribYPos = hPanelScaled/2 - hRib/2
    }

    nested.rect(wPanel, hRib).attr({
        fill: col.rib,
        y: ribYPos
    })

    add_top_space(topPanel, wPanel, nested)
    add_bottom_space(wPanel, hPanelScaled, nested)
}
/*
create_midrib_panel610('midribPanelTop', 'top', topPanel = true)
create_midrib_panel500('midribPanel1')
create_midrib_panel500('midribPanel2')
create_midrib_panel610('midribPanelBottom', 'bottom')
*/
function create_midrib_panel610(dom_id, position, wPanel, topPanel = false) {
    wPanel *= scale
    hPanel = 610
    hRib = 16
    let ribDistance

    var svgCont = SVG(dom_id).size(wPanel, hPanel).attr({
        class: ['midrib', 'panel'],
        'data-panel-type': 'midrib',
        'data-panel-height': hPanel
    })
    var panel = svgCont.rect(wPanel, hPanel).attr({ 
        fill: col.RAL9002,
        y: 0,
        x: 0
    })
    var nested = svgCont.nested()

    if (position === 'top') {
        ribDistanceTop = 360
    } else if (position === 'bottom') {
        ribDistanceTop = 250
    }

    nested.rect(wPanel, hRib).attr({
        fill: col.rib,
        y: (ribDistanceTop - hRib/2)
    })

    add_top_space(topPanel, wPanel, nested)
    add_bottom_space(wPanel, hPanel, nested)
}

function create_toprib_panel(dom_id, hPanel, wPanel, topPanel= false) {
    wPanel *= scale
    hPanelScaled = hPanel * scale
    hRib = 16 
    hRib *= scale

    var svgCont = SVG(dom_id).size(wPanel, hPanelScaled).attr({
        class: ['toprib', 'panel'],
        'data-panel-type': 'toprib',
        'data-panel-height': hPanel
    })
    var panel = svgCont.rect(wPanel, hPanelScaled).attr({ 
        fill: col.RAL9002,
        y: 0,
        x: 0
    })
    var nested = svgCont.nested()

    // jei topPanel, ribas nepaisomas, dedamas slickas, jo aukstis yra 100mm mazesnis uz paskutini
    if (!topPanel) {
        // jei 500 arba 610, tai tiketina, kad bus vidury ir nepjauti
        if (hPanel === 500 || hPanel === 610) {
            switch (hPanel) {
                case 500 :
                    topribYPos = 105 * scale - (hRib / 2)
                    break;
                case 610 : 
                    topribYPos = 104 * scale - (hRib / 2)
                    break;
                    
            }
        } else if (hPanel < 500) {
            topribYPos = (105 * scale) - (hRib / 2)
        } else if (hPanel < 610) {
            topribYPos = (104 * scale) - (hRib / 2)
        }
        var rib = nested.rect(wPanel, hRib).attr({
            fill: col.rib, 
            y: topribYPos
        })
    }
    add_top_space(topPanel, wPanel, nested)
    add_bottom_space(wPanel, hPanelScaled, nested)
}
/*
create_toprib_panel('topribPanelTop', 500, topPanel = true)
create_toprib_panel('topribPanel1', 500)
create_toprib_panel('topribPanelBottom', 610)
*/

function create_microrib_panel(dom_id, hPanel, wPanel, topPanel= false) {
    wPanel *= scale
    hRib = 10 / 2
    hRib *= scale
    hPanelScaled = hPanel * scale

    var svgCont = SVG(dom_id).size(wPanel, hPanelScaled).attr({
        class: 'microrib',
        'data-panel-type': 'microrib',
        'data-panel-height': hPanel
    })
    var panel = svgCont.rect(wPanel, hPanelScaled).attr({ 
        fill: col.RAL9002,
        y: 0,
        x: 0
    })
    var nested = svgCont.nested()

    var rib = nested.rect(wPanel, hRib).attr({
        fill: col.rib,
        y: (hRib / 2)
    })
    let ribCount = Math.ceil((hPanel - 2.5) / 10)
    console.log(ribCount)
    for (let i = 1; i < ribCount; i++) {
        nested.use(rib).move(0, 10 * scale * i)
    }

    add_top_space(topPanel, wPanel, nested)
    add_bottom_space(wPanel, hPanelScaled, nested)
}

/*
create_microrib_panel('microribPanelTop', 500, topPanel = true)
create_microrib_panel('microribPanel1', 500)
create_microrib_panel('microribPanelBottom', 500)
*/
function create_macrorib_panel(dom_id, hPanel, wPanel, topPanel= false) {
    wPanel *= scale
    hRib = 20 / 2
    hRib *= scale
    hPanelScaled = hPanel * scale

    var svgCont = SVG(dom_id).size(wPanel, hPanelScaled).attr({
        class: 'macrorib',
        'data-panel-type': 'macrorib',
        'data-panel-height': hPanel
    })
    var panel = svgCont.rect(wPanel, hPanelScaled).attr({ 
        fill: col.RAL9002,
        y: 0,
        x: 0
    })
    var nested = svgCont.nested()
   
    var rib = nested.rect(wPanel, hRib).attr({
        fill: col.rib,
        y: (hRib / 2)
    })
    let ribCount = Math.ceil((hPanel - 5) / 20)
    for (let i = 1; i < ribCount; i++) {
        nested.use(rib).move(0, 20 * scale * i)
    }

    add_top_space(topPanel, wPanel, nested)
    add_bottom_space(wPanel, hPanelScaled, nested)
}

// let el = document.getElementsByClassName('topribSVG')[0]
// var style = window.getComputedStyle(el)